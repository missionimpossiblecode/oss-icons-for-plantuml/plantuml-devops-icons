# PlantUML Git Icons

Git graphics for use in PlantUML.

# Icon Sets

| Set Name      | Gallery                                            | Source                                                | License                                                      |
| ------------- | -------------------------------------------------- | ----------------------------------------------------- | ------------------------------------------------------------ |
| Feather Icons | [Gallery](https://github.com/feathericons/feather) | [Git Source](https://github.com/feathericons/feather) | [MIT](https://github.com/feathericons/feather/blob/master/LICENSE) |
|               |                                                    |                                                       |                                                              |
|               |                                                    |                                                       |                                                              |
